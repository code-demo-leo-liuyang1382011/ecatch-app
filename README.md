# Overview
This is the App client side of eCatch, information from https://gitlab.com/code-demo-leo-liuyang1382011/ecatch-api/-/blob/master/README.md

# Technical stacks
1. Ionic 5 
2. Angular 8
3. Capacitor
4. NgRx (Redux pattern)
5. Fastlane (For automatic management of iOS and Android project)
6. Gitlab CI

# Note
The main business logic has been removed selectively, and so it is not a running project. Showing the code sample is the main purpose.

# EcatchApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.4.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## Pulling the environment (Fisher-app)
Prerequisites
  - Install aws-cli and setup default and vfa profile.
    ```
    aws configure
    aws configure --profile vfa
    ```
Fetching the initial .env to set target env in environment file
(Copy the generated env file in .envs folder to the root of the project and remove the local name.)

    ./get-env.sh -e local -b develop

Fetch envs-json.app and copy to projects/fisher-app/assets or projects/admin-webapp/assets 

    ./get-env-app.sh -b develop -p fisher-app

Creating the environment file (node scripts/update-environments.js)
    
    npm run update-environments fisher-app

Update environment.ts file 

    change production to false and targetEnv to dev
