#!/bin/bash

function deploy {
    echo "start of deploy"
    VERSION=$1

    ZIP=$VERSION.zip
    echo "deploy $VERSION into $S3_APP"
    rm -f $VERSION.zip
    echo "download $ZIP"
    aws s3 cp $S3_ARTIFACTS/web/$VERSION.zip . || { echo "s3 cp failed"; exit 1; }

    echo "unzip $ZIP"
    unzip -o -q $ZIP || { echo "unzip failed"; exit 1; }

    # echo "update environment"
    # ./copy-env.sh -t dist || { echo "copy env failed"; exit 1; }

    # echo "cd dist"
    # cd dist

#    APP=aqua-assist
#    TARGET_DIR=/var/www/$TARGET_ENV-$APP
#    echo "cp to $TARGET_DIR"
#    sudo mkdir -p $TARGET_DIR
#    sudo cp -r ./* $TARGET_DIR/
#    sudo chown -R jenkins:jenkins $TARGET_DIR
    TARGET_DIR=/var/jenkins/ridonline_$TARGET_ENV/ridonline_$TARGET_ENV/ridonlineprivate/dist/bulk-upload/
    echo "cp to $TARGET_DIR"
    sudo mkdir -p $TARGET_DIR
    sudo cp -r dist/bulk-upload/* $TARGET_DIR
    sudo chown -R jenkins:jenkins $TARGET_DIR

    # cd ..

    echo "remove $ZIP"
    rm -fr $ZIP dist
}

function restart-nginx {
    TARGET_DIR=$1
    echo "cd to $TARGET_DIR"
    cd $TARGET_DIR || { echo "cd failed"; exit 1; }

    echo "stop nginx"
    docker-compose stop nginx  || { echo "stop failed"; exit 1; }

    echo "start nginx"
    docker-compose start nginx  || { echo "start failed"; exit 1; }
}
