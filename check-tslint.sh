#!/bin/bash

echo "setup nvm as $WHO"
source ~/.bash_profile
nvm use

echo "npm install"
npm i

echo "check tslint"
ng lint || { echo "ng lint failed"; exit 1; }
