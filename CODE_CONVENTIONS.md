## NgRx

#### Selector vs Helper function names

Follow the convention of get* OR is* for private helpers and then select* for anything that is an actual selector.

``export const isTripSetup = (state: ITripState) => !isNilOrEmpty(state.dateSubmitted);``

``export const getViewStatus = (state: IState) => state.viewStatus;``

``export const selectCancelTripViewStatus = createSelector(
    selectCancelTripState,
    fromCancelTrip.getViewStatus,
  );``

---

## Commit Message Format

Each commit message consists of a header, a body and a footer. The header has a special format that includes a type, a scope and a subject:

```
<type>(<scope>): <subject>
<BLANK LINE>
<body>
<BLANK LINE>
<footer>
```

**The header is mandatory and the scope of the header is optional.**

Any line of the commit message cannot be longer 100 characters!

The footer should contain a closing reference to an issue if any.

Samples:

```
feat(common): add common asset folder and vic-fish logo

trying to put common assets in root level project so we can share it between fisher-app and admin-app

https://spatialvision.atlassian.net/browse/EC-188
```
```
fix(release): need to depend on latest rxjs and zone.js

The version in our package.json gets copied to the one we publish, and users need the latest of these.
```

#### Revert

If the commit reverts a previous commit, it should begin with revert: , followed by the header of the reverted commit. In the body it should say: This reverts commit <hash>., where the hash is the SHA of the commit being reverted.

#### Type

Must be one of the following:

* build: Changes that affect the build system (example scopes: build scripts, npm)
* ci: Changes to our CI configuration files and scripts
* docs: Documentation only changes
* feat: A new feature
* fix: A bug fix
* perf: A code change that improves performance
* refactor: A code change that neither fixes a bug nor adds a feature
* style: Changes that do not affect the meaning of the code (white-space, formatting, missing semi-colons, etc)
* test: Adding missing tests or correcting existing tests

#### Scope

The scope should be the name of the npm package affected (as perceived by the person reading the changelog generated from commit messages.

The following is the list of supported scopes:

* fisher-app - used for changes that only affect the fisher app
* admin-webapp - used for changes that only affect the admin-webapp
* common - used for changes that only affect both admin-webapp and fisher app

#### Body

Just as in the subject, use the imperative, present tense: "change" not "changed" nor "changes". 
**The body should include the motivation for the change and contrast this with previous behavior.**

#### Footer

The footer should contain any information about Breaking Changes and is also the place to reference JIRA issues that this commit is related to (please use the full url like https://spatialvision.atlassian.net/browse/EC-188).

Breaking Changes should start with the word BREAKING CHANGE: with a space or two newlines. The rest of the commit message is then used for this.

A detailed explanation can be found in this <a href="https://docs.google.com/document/d/1QrDFcIiPjSLDn3EL15IJygNPiHORgU1_OOAqWjiDU5Y/edit#" rel="nofollow">document</a>.
