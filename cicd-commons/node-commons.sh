#!/bin/bash
while [[ $# -gt 1 ]]
do
key="$1"

case $key in
    -s|--source)
    NVM_SOURCE="$2"
    shift # past argument
    ;;
    *)
          # unknown option
    ;;
esac
shift # past argument or value
done

echo "NVM_SOURCE: $NVM_SOURCE"
if [ -z "$NVM_SOURCE" ]; then
    NVM_SOURCE=~/.nvm/nvm.sh
    echo "NVM_SOURCE not specified, setting default"
fi

function install-nvm {
    curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.2/install.sh | bash
}

function init-nvmrc-source {
    NVM_SOURCE=$1
    echo "nvm use with .nvmrc, source nvm with NVM_SOURCE: $NVM_SOURCE"
    source $NVM_SOURCE || { echo "source failed"; exit 1; }
    nvm use || { echo "nvm use failed"; exit 1; }
}

function init-npm {
    echo "run npm install"
    npm install || { echo 'install failed'; exit 1; }

    #echo "run npm update"
    #npm update
}
