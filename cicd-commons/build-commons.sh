#!/bin/bash
function app-version {
    cd mobile
    export APP_VERSION=`node version.js`
    echo "export APP_VERSION: $APP_VERSION"
    cd ..
}

function env-subst {
	SOURCE_ENV=./$1
	TARGET_DIR=$2
	TARGET_FILE=$3
	TARGET=$TARGET_DIR/$TARGET_FILE

	echo "mkdri $TARGET_DIR if not exists"
	mkdir -p $TARGET_DIR

	echo "copy $SOURCE_ENV to $TARGET"
	envsubst < $SOURCE_ENV > $TARGET || { echo "envsubst failed"; exit 1; }
}

function copy-app-vars {
    env-subst $1 $2 app-variables.js
}

function copy-callback {
	env-subst callback.html $1 callback.html
}

function copy-envs-json {
    env-subst envs.json $1 envs.json
}

function copy-environment {
    env-subst $1 $2 $3
    #env-subst environment.prod.ts $1
}

