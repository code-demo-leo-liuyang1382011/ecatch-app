#/bin/bash
source ./node-commons.sh
init-nvmrc-source $NVM_SOURCE || { echo "init-nvm failed"; exit 1; }

export S3_ARTIFACTS="ecatch-artifacts.vfa.vic.gov.au"
export S3_ARTIFACTS_PREFIX="webapp"
export DOMAIN_NAME="vfa.vic.gov.au"
export WEBAPP_VERSION=`node scripts/version` || { echo "api-version failed"; exit 1; }
export WEBAPP_ZIP="$WEBAPP_VERSION.zip"
export AWS_PROFILE="vfa"

if [ -z ${TARGET_ENV} ]
then
    echo "Provide TARGET_ENV"
    exit 1;
fi

export S3_WEBAPP="${TARGET_ENV}-ecatch.${DOMAIN_NAME}"

function zip-append-file {
    ZIP=$1
    APPEND_FILE=$2
    zip -ur $ZIP $APPEND_FILE || { echo "Zip append file to ZIP failed"; exit 1; }
}

function zipit {
    ZIP=$1
    CONTENTS=$2
    echo "zip $ZIP with $CONTENTS in $(pwd)"
    rm -fr $ZIP
    cmd="zip -q -r $ZIP $CONTENTS -x *.git* -x '*scripts*' \
    -x *.tscmultiwatch* \
    -x *.sh -x *.yaml -x $WEBAPP_ZIP"
    echo $cmd
    eval $cmd
}