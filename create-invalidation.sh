#!/bin/bash
source projects/admin-webapp/.env
if [ -z $CLOUD_FRONT_ID ]
then
    echo "CLOUD_FRONT_ID not set"
    exit 1;
fi
echo "create-invalidation for CLOUD_FRONT_ID: $CLOUD_FRONT_ID"
aws cloudfront create-invalidation --distribution-id $CLOUD_FRONT_ID \
 --invalidation-batch file://invalidation-batch.json --profile vfa