import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { ErrorMessageModule } from '../error-message/error-message.module';
import { SearchNSelectModule } from '../search-n-select/search-n-select.module';
import { SearchNSelectInputComponent } from './search-n-select-input.component';

@NgModule({
  declarations: [SearchNSelectInputComponent],
  exports: [SearchNSelectInputComponent],
  imports: [
    CommonModule,
    IonicModule,
    ErrorMessageModule,
    SearchNSelectModule,
  ],
})
export class SearchNSelectInputModule { }
