import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';
import { isNilOrEmpty, prop } from '../ramda-functions';
import { SearchNSelectComponent } from '../search-n-select/search-n-select.component';
import { SearchNSelect } from '../search-n-select/SearchNSelect';
import { SearchNSelectInputType } from './SearchNSelectInputType';
@Component({
  selector: 'ecatch-search-n-select-input',
  templateUrl: './search-n-select-input.component.html',
  styleUrls: ['./search-n-select-input.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SearchNSelectInputComponent {

  @Input() public searchNSelectInputType: SearchNSelectInputType = SearchNSelectInputType.Button;
  @Input() public label: string;
  @Input() public buttonText: string;
  @Input() public labelPosition = 'stacked';
  @Input() public placeholder: string;
  @Input() public value: string;
  @Input() public errorMessage: string;
  @Input() public showErrorMessage = false;
  @Input() public disabled = false;

  // tslint:disable-next-line: no-any
  @Input() public items$: Observable<any>;
  @Input() public searchComponentProps: SearchNSelect;

  // tslint:disable-next-line: no-any
  @Output() public select: EventEmitter<any> = new EventEmitter<any>();

  public SearchNSelectInputType = SearchNSelectInputType;
  private isModalOpen = false;
  constructor(
    private modalController: ModalController,
  ) { }

  public showSearchNSelect() {
    if (this.isModalOpen || this.disabled) {
      return;
    }
    this.isModalOpen = true;
    this.items$
      .pipe(take(1))
      .subscribe(async (items) => {
        const searchModal = await this.modalController.create({
          component: SearchNSelectComponent,
          componentProps: {
            ...this.searchComponentProps,
            items,
          },
        });
        searchModal.onDidDismiss().then(this.onSelectDismiss);
        await searchModal.present();
      });
  }

  private onSelectDismiss = (selection) => {
    this.isModalOpen = false;
    if (isNilOrEmpty(prop('data', selection))) {
      return;
    }
    this.select.next(selection.data);
  }
}
