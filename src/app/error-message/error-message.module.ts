import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ErrorMessageComponent } from './error-message.component';

const COMPONENTS = [ErrorMessageComponent];
@NgModule({
  declarations: COMPONENTS,
  exports: COMPONENTS,
  imports: [
    CommonModule,
  ],
})
export class ErrorMessageModule { }
