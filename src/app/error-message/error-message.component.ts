import { Component, Input } from '@angular/core';

@Component({
  selector: 'ecatch-error-message',
  templateUrl: './error-message.component.html',
  styleUrls: ['./error-message.component.scss'],
})
export class ErrorMessageComponent {
  @Input() public error: string;
}
