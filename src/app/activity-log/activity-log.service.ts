import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { delay, retryWhen, switchMap, take, tap } from 'rxjs/operators';
import { EnvsService } from '../shared/envs.service';

const MAXIMUM_RETRY_COUNT = 5;
export const RETRY_DELAY = 4_000;

@Injectable({
              providedIn: 'root',
            })
export class ActivityLogService {

  constructor(private http: HttpClient, private envsService: EnvsService) {

  }

  public push(logs): Observable<object> {
    const http$ = this.envsService.apiUrl()
      .pipe(
        switchMap(url => this.http.post(`${url}/activity-log`, logs)),
      );

    return http$
      .pipe(
        retryWhen(errors => errors
          .pipe(
            tap(error => {
              if (error.status < 500) {
                throw error;
              }
              console.log('activity log sync Retrying...');
            }),
            delay(RETRY_DELAY),
            take(MAXIMUM_RETRY_COUNT),
          ),
        ),
      );
  }
}
