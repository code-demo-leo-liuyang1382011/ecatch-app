import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import {
  catchError, delay,
  filter,
  map,
  switchMap,
  withLatestFrom,
} from 'rxjs/operators';
import { AuthStateSelector } from '../auth/auth.selector';
import { push, pushFailed, pushSuccess } from './activity-log.actions';
import { IState, selectPushPending, selectUnpushedLogs } from './activity-log.reducer';
import { ActivityLogService, RETRY_DELAY } from './activity-log.service';

@Injectable()
export class ActivityLogEffects {

  public push$ = createEffect(() => this.actions$.pipe(
    ofType(push),
    delay(RETRY_DELAY),
    withLatestFrom(
      this.authStateSelector.isLoggedIn(),
      this.activityLogStore.select(selectUnpushedLogs),
      this.activityLogStore.select(selectPushPending),
    ),
    filter(([_, isLoggedIn, logs, pushPending]) => isLoggedIn && logs.length > 0 && pushPending),
    switchMap(([_, _isLoggedIn, logs]) => this.activityLogService.push(logs)),
    map(_ => pushSuccess()),
    catchError((e: HttpErrorResponse, caught) => {
      this.activityLogStore.dispatch(pushFailed({ error: e.message }));

      return caught;
    }),
  ));

  constructor(private actions$: Actions,
              private activityLogStore: Store<IState>,
              private activityLogService: ActivityLogService,
              private authStateSelector: AuthStateSelector) {
  }

}
