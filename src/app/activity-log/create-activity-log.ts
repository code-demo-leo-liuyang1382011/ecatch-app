import { ActivityType } from 'fisher-model';
import { curry } from '../ramda-functions';

// tslint:disable-next-line:max-func-args
const _createActivityLog = (page: string, action: string,
                            type: ActivityType, pfn: number,
                            message: string) => ({
  pfn,
  message,
  type,
  page,
  action,
  timestamp: '',
});

export const createActivityLog = curry(_createActivityLog);
