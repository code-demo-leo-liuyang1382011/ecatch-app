import { Action, createFeatureSelector, createReducer, createSelector, on } from '@ngrx/store';
import { IActivityLog } from 'fisher-model';
import * as moment from 'moment';
import { DATETIME_FORMAT } from '../shared/date-formats';
import { isDefined } from '../shared/isDefined';
import { log, pushFailed, pushSuccess } from './activity-log.actions';
import { filterExpiredLogs } from './filter-expired-logs';

export const activityLogsFeatureKey = 'activityLogs';

export interface IState {
  logs: IActivityLog[];
  lastPushed: string;
  pushPending: boolean;
  error: string;
}

export const initialState: IState = {
  logs: [],
  lastPushed: undefined,
  pushPending: false,
  error: undefined,
};

const activityLogReducer = createReducer(
  initialState,
  on(log, (state, action) => {
    const auditLog: IActivityLog = {
      ...action.data,
      timestamp: moment().format(DATETIME_FORMAT),
    };
    const filteredLogs = filterExpiredLogs(state.logs, state.lastPushed);

    return { ...state, logs: [...filteredLogs, auditLog], pushPending: true };
  }),
  on(pushSuccess, (state): IState => {
    return { ...state, pushPending: false, lastPushed: moment().format(DATETIME_FORMAT) };
  }),
  on(pushFailed, (state, action) => {
    return { ...state, pushPending: true, error: action.error };
  }),
);

export function reducer(state: IState | undefined, action: Action) {
  return activityLogReducer(state, action);
}

export const selectActivityLog = createFeatureSelector<IState>(activityLogsFeatureKey);

export const selectLogs = createSelector(selectActivityLog,
                                         (state: IState) => state.logs,
);

export const selectPushPending = createSelector(
  selectActivityLog,
  (state: IState) => state.pushPending,
);

export const selectUnpushedLogs = createSelector(
  selectActivityLog,
  (state: IState) => {
    if (!isDefined(state.lastPushed)) {
      return state.logs;
    }

    return state.logs
      .filter(l => filterByTimestamp(state.lastPushed, l));
  },
);

const filterByTimestamp = (timestamp: string, activityLog: IActivityLog): boolean =>
  moment(activityLog.timestamp).isAfter(moment(timestamp));
