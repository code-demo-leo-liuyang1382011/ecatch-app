import { createAction, props } from '@ngrx/store';
import { IActivityLog } from 'fisher-model';

export const log = createAction(
  '[ActivityLog] Log',
  props<{ data: IActivityLog }>(),
);

export const push = createAction(
  '[ActivityLog] Push',
);

export const pushSuccess = createAction(
  '[ActivityLog] Push Success',
);

export const pushFailed = createAction(
  '[ActivityLog] Push Failed',
  props<{ error: string }>(),
);
