import { IActivityLog } from 'fisher-model';
import * as moment from 'moment';
import { DATETIME_FORMAT } from '../shared/date-formats';
import { filterExpiredLogs } from './filter-expired-logs';

describe('filterExpiredLogs', () => {
  const activityLogs = [
    {
      timestamp: moment().subtract(20, 'day').format(DATETIME_FORMAT),
    },
    {
      timestamp: moment().subtract(1, 'day').subtract(1, 'month').format(DATETIME_FORMAT),
    },
    {
      timestamp: moment().subtract(1, 'hour').format(DATETIME_FORMAT),
    },
  ] as IActivityLog[];

  it('should return only two items if lastPushed is just now', () => {
    const lastPushed = moment().format(DATETIME_FORMAT);
    const filteredLogs = filterExpiredLogs(activityLogs, lastPushed);
    expect(filteredLogs.length).toBe(2);
  });

  it('should return all three items if lastPushed was two months back', () => {
    const lastPushed = moment().subtract(2, 'months').format(DATETIME_FORMAT);
    const filteredLogs = filterExpiredLogs(activityLogs, lastPushed);
    expect(filteredLogs.length).toBe(3);
  });
});
