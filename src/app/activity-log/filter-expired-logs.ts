import { IActivityLog } from 'fisher-model';
import * as moment from 'moment';

export const filterExpiredLogs = (logs: IActivityLog[], lastPushed: string) => {
  return logs.filter(activityLog => {
    const oneMonthBackMoment = moment().subtract(1, 'month');
    const isLogOlderThanOneMonth = moment(activityLog.timestamp).isBefore(oneMonthBackMoment);

    const isLogPushed = moment(activityLog.timestamp)
      .isBefore(moment(lastPushed));

    return !isLogOlderThanOneMonth || !isLogPushed;
  });
};
