import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { ActivityLogEffects } from './activity-log.effects';
import * as fromActivityAudit from './activity-log.reducer';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature(fromActivityAudit.activityLogsFeatureKey, fromActivityAudit.reducer),
    EffectsModule.forFeature([ActivityLogEffects]),
  ],
})
export class ActivityLogModule { }
