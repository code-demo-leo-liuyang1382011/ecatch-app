import { ChangeDetectionStrategy, Component, EventEmitter, OnInit, Output } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { environment } from '../../environments/environment';
import { SettingsActionDispatcher } from '../settings/settings.dispatcher';
import { SettingsStateSelector } from '../settings/settings.selector';

@Component({
  selector: 'app-version',
  templateUrl: './app-version.component.html',
  styleUrls: ['./app-version.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppVersionComponent implements OnInit {
  public version: string;
  public buildNumber: string;
  public debugClickCount = 0;
  public maxDebugClickCount = 14;
  public debugEnabled = false;
  public halfWayMessageShown = false;

  @Output() public enable = new EventEmitter<void>();

  constructor(private settingsActionDispatcher: SettingsActionDispatcher,
              private toastCtrl: ToastController,
              public settingsStateSelector: SettingsStateSelector) {
  }

  public clickVersion(): void {
    if (this.debugEnabled) {
      this.enable.emit();

      return;
    }
    this.debugClickCount++;
    if (this.debugClickCount > (this.maxDebugClickCount / 2) && !this.halfWayMessageShown) {
      this.halfWayMessageShown = true;
      this.presentToast('You are almost there! Tap Tap!');
    }
    if (this.debugClickCount < this.maxDebugClickCount) {
      return;
    }
    this.enableDebug();
  }

  public enableDebug(): void {
    this.presentToast('Debug mode enabled!');
    this.enable.emit();
    this.settingsActionDispatcher.enableDebug();
    this.debugClickCount = 0;
    this.halfWayMessageShown = false;
    this.debugEnabled = true;
  }

  private async presentToast(message: string) {
    const toast = await this.toastCtrl.create({
      message,
      position: 'top',
      duration: 3000,
    });
    toast.present();
  }

  public ngOnInit(): void {
    this.version = environment.version;
    this.buildNumber = environment.buildNumber;
    this.settingsStateSelector
      .debug()
      .subscribe(debug => this.debugEnabled = debug);
  }
}
