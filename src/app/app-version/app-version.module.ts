import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { AppVersionComponent } from './app-version.component';

export const COMPONENTS = [AppVersionComponent];

@NgModule({
  imports: [
    CommonModule,
    IonicModule,
  ],
  declarations: COMPONENTS,
  exports: COMPONENTS,
  entryComponents: COMPONENTS,
})
export class AppVersionModule { }
