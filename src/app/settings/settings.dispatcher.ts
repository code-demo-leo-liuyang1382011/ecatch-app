import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { ISettings } from './Settings';
import { ChangeEnv, EnableDebug } from './settings.actions';

@Injectable({
  providedIn: 'root',
})
export class SettingsActionDispatcher {

  constructor(private store: Store<ISettings>) {
  }

  public changeEnv(targetEnv: string): void {
    this.store.dispatch(new ChangeEnv(targetEnv));
  }

  public enableDebug(): void {
    this.store.dispatch(new EnableDebug());
  }
}
