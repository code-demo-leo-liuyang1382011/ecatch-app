import { Action } from '@ngrx/store';
import { EnvMap } from './Settings';

export enum SettingsActionsType {
  LoadEnvs = '[Settings] Load Envs',
  ChangeEnv = '[Settings] Change Env',
  EnableDebug = '[Settings] Enable Debug',
}

export class LoadEnvs implements Action {
  public readonly type = SettingsActionsType.LoadEnvs;

  constructor(public payload: EnvMap) {}
}

export class ChangeEnv implements Action {
  public readonly type = SettingsActionsType.ChangeEnv;

  constructor(public payload: string) {}
}

export class EnableDebug implements Action {
  public readonly type = SettingsActionsType.EnableDebug;
}

export type SettingsActions = ChangeEnv
  | EnableDebug
  | LoadEnvs;
