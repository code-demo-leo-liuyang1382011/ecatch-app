import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { map, switchMap } from 'rxjs/operators';
import { bootstrapApp } from '../root/root.actions';
import { EnvsService } from '../shared/envs.service';
import {
  LoadEnvs,
} from './settings.actions';

@Injectable()
export class SettingsEffects {

  public loadEnvs$ = createEffect(() =>
          this.actions$
            .pipe(
              ofType(bootstrapApp),
              switchMap(() => this.envs.load()),
              map(envs => new LoadEnvs(envs)),
            ));

  constructor(private actions$: Actions,
              private envs: EnvsService) {
  }
}
