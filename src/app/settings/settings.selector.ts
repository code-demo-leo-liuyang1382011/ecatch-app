import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { filter, map, switchMap } from 'rxjs/operators';
import { IState, selectDebugMode, selectEnvironments, selectTargetEnv } from './settings.reducer';
@Injectable({
  providedIn: 'root',
})
export class SettingsStateSelector {
  constructor(private store: Store<IState>) {}

  public targetEnv = () => this.store.select(selectTargetEnv);

  public debug = () => this.store.select(selectDebugMode);

  public environments = () => this.store.select(selectEnvironments)
    .pipe(
      filter(environments => Object.keys(environments).length > 0),
    )

  public apiUrl = () => this.targetEnv()
    .pipe(
      switchMap(targetEnv =>
        this.environments()
          .pipe(
            map(environments => environments[targetEnv].apiUrl),
          ),
      ),

    )
}
