import { createFeatureSelector, createSelector } from '@ngrx/store';
import { environment } from '../../environments/environment';
import { EnvMap } from './Settings';
import { SettingsActions, SettingsActionsType } from './settings.actions';

export interface IState {
  targetEnv: string;
  environments: EnvMap;
  debugMode: boolean;
}
export const initialState: IState = {
  targetEnv: environment.targetEnv,
  environments: {},
  debugMode: false,
};

export function reducer(state = initialState, action: SettingsActions): IState {
  switch (action.type) {
    case SettingsActionsType.LoadEnvs:
      return {
        ...state,
        environments: action.payload,
       };

    case SettingsActionsType.EnableDebug:
      return {
        ...state,
        debugMode: true,
       };

    case SettingsActionsType.ChangeEnv:
       return {
         ...state,
         targetEnv: action.payload,
       };

    default:
      return state;
  }
}

export const selectSettingsState = createFeatureSelector<IState>('settings');

export const selectTargetEnv = createSelector(selectSettingsState, state => state.targetEnv);

export const selectEnvironments = createSelector(selectSettingsState, state => state.environments);

export const selectDebugMode = createSelector(selectSettingsState, state => state.debugMode);
