import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { NgxJsonViewerModule } from 'ngx-json-viewer';
import { SettingsEffects } from './settings.effects';
import * as fromSettings from './settings.reducer';
@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    StoreModule.forFeature('settings', fromSettings.reducer),
    EffectsModule.forFeature([SettingsEffects]),
    NgxJsonViewerModule,
  ],
})
export class SettingsModule { }
