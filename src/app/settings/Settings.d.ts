export interface ISettings {
  name: string;
  lastUpdate: string;
}

export interface Env {
  name: string;
  apiUrl: string;
  isDebugMode: boolean;
}

export interface EnvMap {
  [key: string]: Env;
}
