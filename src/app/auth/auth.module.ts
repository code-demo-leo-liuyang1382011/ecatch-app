import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { AuthEffects } from './auth.effects';
import * as fromAuth from './auth.reducer';
import { AuthStateSelector } from './auth.selector';
import { AuthService } from './auth.service';
import { JwtInterceptor } from './jwt.interceptor';
import { AuthTokenInterceptor } from './token.interceptor';

@NgModule({
            imports: [
              CommonModule,
              StoreModule.forFeature('auth', fromAuth.reducer),
              EffectsModule.forFeature([AuthEffects]),
              HttpClientModule,
            ],
            providers: [
              AuthService,
              AuthStateSelector,
              {
                provide: HTTP_INTERCEPTORS,
                useClass: AuthTokenInterceptor,
                multi: true,
              },
              {
                provide: HTTP_INTERCEPTORS,
                useClass: JwtInterceptor,
                multi: true,
              },

            ],
          })
export class AuthModule {
}
