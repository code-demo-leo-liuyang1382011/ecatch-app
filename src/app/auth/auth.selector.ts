import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { isDefined } from '../shared/isDefined';
import { IState, selectToken } from './auth.reducer';

@Injectable()
export class AuthStateSelector {

  constructor(private store: Store<IState>) {
  }

  public token(): Observable<string> {
    return this.store.select(selectToken);
  }

  public isLoggedIn(): Observable<boolean> {
    return this.store.select(selectToken)
      .pipe(
        map(isDefined),
      );
  }

}
