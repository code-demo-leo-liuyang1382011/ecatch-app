import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map, switchMap, tap } from 'rxjs/operators';
import { LoginActionDispatcher } from '../login/login.dispatcher';
import { EnvsService } from '../shared/envs.service';
import { isDefined } from '../shared/isDefined';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {

  // tslint:disable-next-line:no-any
  public intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return isDefined(request.params.get('isEnvs')) ? next.handle(request) :
      this.envsService.apiUrl()
        .pipe(
          map(apiUrl => request.url.includes(apiUrl)),
          switchMap(isApiUrl => next.handle(request)
            .pipe(
              tap({
                // tslint:disable-next-line:no-any
                next: undefined, error: (error: any) => {
                  if (isApiUrl) {
                    this.checkForExpiredToken(error);
                  }
                },
              }),
            ),
          ));
  }

  constructor(
    public loginActionDispatcher: LoginActionDispatcher,
    private envsService: EnvsService,
  ) {
  }

  private checkForExpiredToken = (err: HttpErrorResponse) => {
    if (err instanceof HttpErrorResponse) {
      if (err.status === 401) {
        this.loginActionDispatcher.logout();
      }
    }
  }
}
