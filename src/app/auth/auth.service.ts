import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import { MESSAGES } from '../../../projects/fisher-app/src/app/message';
import { equals, path } from '../ramda-functions';
import { EnvsService } from '../shared/envs.service';
import { IAuthenticate } from './authenticate';

@Injectable({
  providedIn: 'root',
})

export class AuthService {

  constructor(private http: HttpClient, private envsService: EnvsService) {
  }

  public login(data: IAuthenticate) {
    return this.envsService.apiUrl()
      .pipe(
        map(apiUrl => `${apiUrl}/login`),
        switchMap(loginApi => this.http
          .post(loginApi, data)),
        catchError((error: HttpErrorResponse) => {
          return throwError(path(['error', 'error', 'payload', 'message'], error));
        }),
      );
  }
}
