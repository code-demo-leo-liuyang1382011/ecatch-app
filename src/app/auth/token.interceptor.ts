import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { switchMap, take } from 'rxjs/operators';
import { isNil } from '../ramda-functions';
import { AuthStateSelector } from './auth.selector';

@Injectable()
export class AuthTokenInterceptor implements HttpInterceptor {

  // tslint:disable-next-line:no-any
  public static addAuthHeader(request: HttpRequest<any>, token: string): HttpRequest<any> {
    if (isNil(token)) {
      return request;
    }

    return request.clone({
      setHeaders: {
        Authorization: `Bearer ${token}`,
      },
    });
  }

  // tslint:disable-next-line:no-any
  public intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return this.authStateSelector.token().pipe(
      take(1),
      switchMap(token => {
        const transformedRequest = AuthTokenInterceptor.addAuthHeader(request, token);

        return next.handle(transformedRequest);
      }),
    );
  }

  constructor(public authStateSelector: AuthStateSelector) {
  }
}
