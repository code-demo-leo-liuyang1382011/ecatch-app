import { Action, createFeatureSelector, createReducer, createSelector, on } from '@ngrx/store';
import * as LoginActions from '../login/login.actions';
import * as LogoutActions from '../login/logout.actions';
import * as RootActions from '../root/root.actions';

export interface IState {
  token: string;
}

export const initialState: IState = {
  token: undefined,
};

const authReducer = createReducer(
  initialState,
  on(LoginActions.loginSuccess, (state, action) => ({ ...state, token: action.data.token })),
  on(LogoutActions.logout, (state, action) => initialState),
  on(RootActions.clearCache, (state, action) => initialState),
);

export function reducer(state: IState | undefined, action: Action) {
  return authReducer(state, action);
}

export const selectAuthState = createFeatureSelector<IState>('auth');

export const selectToken = createSelector(selectAuthState,
                                          (state: IState) => state.token,
);
