import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { catchError, concatMap, map, tap } from 'rxjs/operators';
import * as LoginActions from '../login/login.actions';
import * as fromLogin from '../login/login.reducer';
import * as LogoutActions from '../login/logout.actions';
import { clearCache } from '../root/root.actions';
import { AuthService } from './auth.service';

@Injectable()
export class AuthEffects {

  public login$ = createEffect(() =>
                                   this.actions$.pipe(
      ofType(LoginActions.login),
      concatMap(action => this.authService.login(action.data)),
      map((data: { token: string }) => LoginActions.loginSuccess({data})),
      catchError((err, caught) => {
        this.store.dispatch(LoginActions.loginFailure({error: err}));

        return caught;
      }),
    ));

  public logout$ = createEffect(() =>
    this.actions$.pipe(
      ofType(LogoutActions.logout),
      tap(_ => {
        this.router.navigateByUrl('/login');
      }),
      map(clearCache),
    ));

  constructor(private actions$: Actions,
              private authService: AuthService,
              private store: Store<fromLogin.IState>,
              private router: Router,
  ) {
  }
}
