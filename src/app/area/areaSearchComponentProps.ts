import { SearchNSelect } from '../search-n-select/SearchNSelect';

export const areaSearchComponentProps: SearchNSelect = {
  items: [],
  searchAttributeList: ['areaCode'],
  minCharactersBeforeSearch: 2,
  modalTitle: 'Search Area Code',
  placeholderTxt: 'Type area code to search',
  itemNotFoundTxt: 'No Area found',
  display: {
    mainLabel: 'areaCode',
  },
};
