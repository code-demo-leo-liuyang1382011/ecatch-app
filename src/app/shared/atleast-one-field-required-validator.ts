import { FormGroup } from '@angular/forms';
import { isNilOrEmpty } from '../ramda-functions';

export const atleastOneFieldRequiredValidator = (fields: string[]) =>
  (group: FormGroup) =>
    fields
      .map(field => group.get(field))
      .some(control => !isNilOrEmpty(control.value)) ? null : {
        atleastOneRequired: true,
      };
