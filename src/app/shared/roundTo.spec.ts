import { roundTo } from './roundTo';

describe('roundTo', () => {
  it('round to two decimal places by default', () => {
    const result = roundTo(2.332);
    expect(result).toBe(2.33);
  });

  it('round to two three places', () => {
    const result = roundTo(2.3326, 3);
    expect(result).toBe(2.333);
  });

  it('round to two for floating point problem when 4.35 * 3', () => {
    const result = roundTo(4.35 * 3);
    expect(result).toBe(13.05);
  });

  it('round to two for floating point problem when 0.1 * 0.2', () => {
    const result = roundTo(0.1 * 0.2);
    expect(result).toBe(0.02);
  });
});
