export const DATETIME_FORMAT = 'YYYY-MM-DDTHH:mm:ss.SSSZ';
export const DISPLAY_DATETIME_FORMAT = 'd MMM y HH:mm:ss';
export const DISPLAY_DATE_FORMAT = 'd MMM y';
export const TIME_PICKER_DISPLAY_FORMAT = 'HH:mm';
export const DATE_PICKER_DISPLAY_FORMAT = 'D MMM YYYY';
