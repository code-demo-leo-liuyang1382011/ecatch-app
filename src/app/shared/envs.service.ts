import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';
import { EnvMap } from '../settings/Settings';
import { SettingsStateSelector } from '../settings/settings.selector';

@Injectable({
  providedIn: 'root',
})
export class EnvsService {

  constructor(private http: HttpClient, private settingsStateSelector: SettingsStateSelector) {}

  public apiUrl = (): Observable<string> => this.settingsStateSelector.apiUrl().pipe(take(1));

  public load = (): Observable<EnvMap> => this.http.get<EnvMap>('assets/envs-app.json', {
    params: {
      isEnvs: 'true',
    },
  })

}
