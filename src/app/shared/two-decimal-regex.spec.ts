import { twoDecimalRegex } from './two-decimal-regex';

describe('twoDecimalRegex', () => {

  it('matches with 0.23', () => {
    const str = '0.23';
    const pattern = new RegExp(twoDecimalRegex);

    expect(pattern.test(str)).toBeTruthy();
  });

  it('matches with .23', () => {
    const str = '.23';
    const pattern = new RegExp(twoDecimalRegex);

    expect(pattern.test(str)).toBeTruthy();
  });

  it('matches with 100.03', () => {
    const str = '100.03';
    const pattern = new RegExp(twoDecimalRegex);

    expect(pattern.test(str)).toBeTruthy();
  });

  it('does not match with 2.331', () => {
    const str = '0.2331';
    const pattern = new RegExp(twoDecimalRegex);

    expect(pattern.test(str)).toBeFalsy();
  });

  it('does not match with .331231231', () => {
    const str = '.331231231';
    const pattern = new RegExp(twoDecimalRegex);

    expect(pattern.test(str)).toBeFalsy();
  });

  it('matches with -20.32', () => {
    const str = '-20.32';
    const pattern = new RegExp(twoDecimalRegex);

    expect(pattern.test(str)).toBeTruthy();
  });

  it('does not matches with -20.324', () => {
    const str = '-20.324';
    const pattern = new RegExp(twoDecimalRegex);

    expect(pattern.test(str)).toBeFalsy();
  });
});
