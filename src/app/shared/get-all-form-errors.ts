import { FormArray, FormGroup } from '@angular/forms';
import { isNilOrEmpty } from '../ramda-functions';

/**
 * Used for debugging only
 */
// tslint:disable-next-line: no-any
export const getAllFormErrors = (form: FormGroup | FormArray): { [key: string]: any; } | null  => {
  let hasError = false;
  const result = Object.keys(form.controls).reduce((acc, key) => {
      const control = form.get(key);
      const errors = (control instanceof FormGroup || control instanceof FormArray)
          ? {...getAllFormErrors(control), ...control.errors}
          : control.errors;
      if (!isNilOrEmpty(errors)) {
          acc[key] = errors;
          hasError = true;
      }

      return acc;
  },                                               {});

  return hasError ? result : null;
};
