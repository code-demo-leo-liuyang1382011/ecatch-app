import { wholeNumberRegex } from './whole-number-regex';

describe('wholeNumberRegex', () => {

  it('matches with 0', () => {
    const str = '0';
    const pattern = new RegExp(wholeNumberRegex);

    expect(pattern.test(str)).toBeTruthy();
  });

  it('matches with 1', () => {
    const str = '1';
    const pattern = new RegExp(wholeNumberRegex);

    expect(pattern.test(str)).toBeTruthy();
  });

  it('matches with 12000', () => {
    const str = '12000';
    const pattern = new RegExp(wholeNumberRegex);

    expect(pattern.test(str)).toBeTruthy();
  });

  it('not matches with 0.4', () => {
    const str = '0.4';
    const pattern = new RegExp(wholeNumberRegex);

    expect(pattern.test(str)).toBeFalsy();
  });

  it('matches with -20', () => {
    const str = '-20';
    const pattern = new RegExp(wholeNumberRegex);

    expect(pattern.test(str)).toBeTruthy();
  });
});
