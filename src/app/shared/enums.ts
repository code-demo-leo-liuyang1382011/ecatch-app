export enum ViewStatus {
  Initial = 'INITIAL',
  Loading = 'LOADING',
  Success = 'SUCCESS',
  Failure = 'FAILURE',
}

export enum DepthMeasure {
  Metres = 'METRES',
  Fathoms = 'FATHOMS',
}

export enum PotSoaked {
  Day = 'DAY',
  Night = 'NIGHT',
}

export enum TargetSpecies {
  Lobster = 'L',
  Crab = 'C',
  ScaleFish = 'S',
  Octopus = 'O',
}
