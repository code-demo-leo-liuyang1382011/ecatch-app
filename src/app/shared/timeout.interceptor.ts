import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, timeout } from 'rxjs/operators';
import { propEq } from '../ramda-functions';
import { API_TIMEOUT } from './api-timeout';

@Injectable()
export class TimeoutInterceptor implements HttpInterceptor {
  // In case timeout needs to be explicitly different for different API Calls
  // https://stackoverflow.com/a/45986060/4549494

  // tslint:disable-next-line: no-any
  public intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req)
      .pipe(
        timeout(API_TIMEOUT),
        catchError(error => {
          if (propEq('name', 'TimeoutError', error)) {
            return throwError({
              status: 408,
              message: error.message,
            });
          }

          return throwError(error);
        }),
      );
  }
}
