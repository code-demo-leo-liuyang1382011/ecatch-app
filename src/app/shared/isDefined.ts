import { isEmpty, isNil } from '../ramda-functions';

export const isDefined = (val): boolean => !isEmpty(val) && !isNil(val);
