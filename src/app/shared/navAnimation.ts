// tslint:disable-next-line:no-implicit-dependencies no-submodule-imports
import { iosTransitionAnimation } from '@ionic/core/dist/collection/utils/transition/ios.transition';
// tslint:disable-next-line:no-submodule-imports no-implicit-dependencies
import { mdTransitionAnimation } from '@ionic/core/dist/collection/utils/transition/md.transition';

const ECATCH_ABALONE_CATCH_BIN_WEIGHTS = 'ECATCH-ABALONE-CATCH-BIN-WEIGHTS';
const ECATCH_ABALONE_CATCH_BIN_WEIGHTS_SUMMARY = 'ECATCH-ABALONE-CATCH-BIN-WEIGHTS-SUMMARY';
const ECATCH_ABALONE_CATCH_TRIP_STATUS = 'ECATCH-ABALONE-CATCH-TRIP-STATUS';
const wizardPageNames = [
  {
    elementName: ECATCH_ABALONE_CATCH_BIN_WEIGHTS,
    directions: ['forward', 'back'],
  },
  {
    elementName: ECATCH_ABALONE_CATCH_TRIP_STATUS,
    directions: ['back'],
  },
  {
    elementName: ECATCH_ABALONE_CATCH_BIN_WEIGHTS_SUMMARY,
    directions: ['forward'],
  },
];

const findPageByNameAndDirection = (page, baseEl, opts) => {
  if (baseEl && baseEl.enteringEl) {
    return page.elementName === baseEl.enteringEl.tagName &&
      page.directions.includes(baseEl.direction);
  }
  if (opts && opts.enteringEl) {
    return page.elementName === opts.enteringEl.tagName &&
      page.directions.includes(opts.direction);
  }
};

const isWizardPage = (baseEl, opts): boolean =>
  wizardPageNames.some(p => findPageByNameAndDirection(p, baseEl, opts));

export function navAnimation(animation, baseEl, opts) {
  if (isWizardPage(baseEl, opts)) {
    return iosTransitionAnimation(animation, baseEl, opts);
  }

  return mdTransitionAnimation(animation, baseEl, opts);
}
