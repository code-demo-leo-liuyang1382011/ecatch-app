// tslint:disable-next-line: no-implicit-dependencies
import { LoadingOptions } from '@ionic/core';

export const LOADING_DEFAULTS: LoadingOptions = {
  message: 'Submitting',
  spinner: 'crescent',
};
