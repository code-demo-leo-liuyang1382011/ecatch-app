// tslint:disable-next-line: no-implicit-dependencies
import { ToastOptions } from '@ionic/core';

export const TOAST_DEFAULTS: ToastOptions = {
  duration: 3000,
  position: 'top',
  showCloseButton: true,
};
