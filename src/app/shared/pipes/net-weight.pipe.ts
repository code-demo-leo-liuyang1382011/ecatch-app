import { Pipe, PipeTransform } from '@angular/core';
import { complement, defaultTo, isNil } from '../../ramda-functions';
import { roundTo } from '../roundTo';

@Pipe({
  name: 'netWeight',
})
export class NetWeightPipe implements PipeTransform {
  public transform(gross: number, ...deductions: number[]) {
    return getNetWeight(gross, ...deductions);
  }
}

export const getNetWeight = (gross: number, ...deductions: number[]) =>
  roundTo(
    deductions
      .filter(complement(isNil))
      .reduce((net, deduction) => net - deduction, defaultTo(0, gross)),
    2,
  );
