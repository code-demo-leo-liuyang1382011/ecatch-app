import { Pipe, PipeTransform  } from '@angular/core';

@Pipe({
  name: 'join',
})
export class JoinPipe implements PipeTransform {

  public transform (input: string[], character: string = ''): string {

    if (!Array.isArray(input)) {
      return '';
    }

    return input.join(character);
  }
}
