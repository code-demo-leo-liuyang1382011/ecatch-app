import { Pipe, PipeTransform } from '@angular/core';
import { isNil } from '../../ramda-functions';

@Pipe({
  name: 'isDefined',
})
export class IsDefinedPipe implements PipeTransform {
  // tslint:disable-next-line: no-any
  public transform(value: any): boolean {
    return !isNil(value);
  }
}
