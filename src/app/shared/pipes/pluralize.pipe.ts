import { Pipe, PipeTransform } from '@angular/core';
import { equals } from '../../ramda-functions';

/**
 * Usages:
 *  - result{{ items | pluralize }}
 *  - {{ items | pluralize: 'results' : 'result' }}
 */
@Pipe({
  name: 'pluralize',
})
export class PluralizePipe implements PipeTransform {
  // tslint:disable-next-line: no-any
  public transform(items: any[] | number, pluralForm = 's', singularForm = ''): string {
    if (equals(typeof items, 'number')) {
      return items > 1 ? pluralForm : singularForm;
    } else if (Array.isArray(items)) {
      return items.length > 1 ? pluralForm : singularForm;
    }

    return singularForm;

  }
}
