import { NgModule } from '@angular/core';
import { IsDefinedPipe } from './is-defined.pipe';
import { IsDiveDetailsNotEnteredPipe } from './is-dive-details-not-entered.pipe';
import { JoinPipe } from './join.pipe';
import { NetWeightPipe } from './net-weight.pipe';
import { PluralizePipe } from './pluralize.pipe';

const PIPES = [
  PluralizePipe,
  IsDefinedPipe,
  JoinPipe,
  NetWeightPipe,
  IsDiveDetailsNotEnteredPipe,
];
@NgModule({
  declarations: PIPES,
  exports: PIPES,
})
export class SharedPipesModule {}
