import { Pipe, PipeTransform } from '@angular/core';
import { IAbaloneActivity } from 'fisher-model';
// tslint:disable-next-line:max-line-length
import { isDiveDetailsEnteredForFishingActivity } from '../../../../projects/fisher-app/src/app/abalone-catch-trip-status/is-dive-details-entered-for-fishing-activity';

@Pipe({
  name: 'isDiveDetailsNotEntered',
})
export class IsDiveDetailsNotEnteredPipe implements PipeTransform {

  public transform(fishingActivity: IAbaloneActivity): boolean {
    return !isDiveDetailsEnteredForFishingActivity(fishingActivity);
  }
}
