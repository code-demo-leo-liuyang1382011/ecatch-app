/**
 * rambda vs ramda
 * rambda is simply faster than ramda.
 * So whatever the function you find in rambda use it, otherwise use ramda
 * If you list in RB then if there is no function, then typescript will complain for you.
 * https://github.com/selfrefactor/rambda#benchmark
 */
import * as RB from 'rambda';
import * as R from 'ramda';
import { roundTo } from './shared/roundTo';

export const {
  and,
  or,
  lensProp,
  lensPath,
  view,
  add,
  set,
  over,
  identity,
  forEach,
  compose,
  when,
  anyPass,
  not,
  propEq,
  join,
  keys,
  ifElse,
  intersection,
  contains,
  difference,
  differenceWith,
  curry,
  reverse,
  propOr,
  uniq,
  uniqBy,
  pickBy,
  comparator,
  gt,
  sort,
} = R;

export const {
  isNil,
  is,
  prop,
  path,
  pathOr,
  equals,
  has,
  isEmpty,
  both,
  either,
  multiply,
  toPairs,
  type,
  find,
  map,
  complement,
  flatten,
  defaultTo,
  range,
  groupBy,
  toLower,
  concat,
} = RB;

export const createLensProp = (val: string) => {
  return lensProp(val);
};

export const toNumber = (val: string): number => {
  console.debug('toNumber: %s, %s', val, isNilOrEmpty(val));

  return isNilOrEmpty(val) ? null : +val;
};

export const isStrNumber = (val: string): boolean => {
  return /^\d+$/.test(val);
};

const isStringNullOrNil = (val: string): boolean => equals('null', val) || equals('undefined', val)  || isNil(val);
export const isNilOrEmpty = either(isStringNullOrNil, isEmpty);
export const propOrZero = propOr(0);

export const sumOfArray = (numbers: number[], roundToDecimal = 2): number =>
  roundTo(numbers.reduce(add, 0), roundToDecimal);
