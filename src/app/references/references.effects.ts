import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, concatMap, map } from 'rxjs/operators';
import { loginSuccess } from '../login/login.actions';
import { loadReferences, loadReferencesFailure, loadReferencesSuccess } from './references.actions';
import { ReferencesService } from './references.service';

@Injectable()
export class ReferencesEffects {

  public loadReferences$ = createEffect(() => this.actions$.pipe(
    ofType(loadReferences, loginSuccess),
    concatMap(() =>
      this.referencesService.load().pipe(
        map(data => loadReferencesSuccess({ data })),
        catchError(() => of(loadReferencesFailure()))),
    ),
  ));

  constructor(private actions$: Actions, private referencesService: ReferencesService) {}

}
