import { Action, createFeatureSelector, createReducer, createSelector, on } from '@ngrx/store';
import { IReference } from 'fisher-model';
import { blacklipSpeciesId, greenlipSpeciesId } from '../licence/abalone-species';
import { equals } from '../ramda-functions';
import { clearCache } from '../root/root.actions';
import { loadReferences, loadReferencesFailure, loadReferencesSuccess } from './references.actions';

export const referencesFeatureKey = 'references';

export type IReferenceState = IReference;

export const initialState: IReferenceState = {
  boats: [],
  reefs: [],
  placesOfLanding: [],
  species: { tep: [], nontep: []},
  receivers: [],
  areaList: [],
};

const referencesReducer = createReducer(
  initialState,

  on(loadReferences, state => state),
  on(loadReferencesSuccess, (state, action) => ({...state, ...action.data})),
  on(loadReferencesFailure, (state, action) => state),
  on(clearCache, (state, action) => initialState),
);

export function reducer(state: IReferenceState | undefined, action: Action) {
  return referencesReducer(state, action);
}

export const selectReferenceState = createFeatureSelector<IReferenceState>(referencesFeatureKey);

export const selectBoats = createSelector(selectReferenceState, state => state.boats);

export const selectReefs = createSelector(selectReferenceState, state => state.reefs);

export const selectPlacesOfLanding = createSelector(selectReferenceState, state => state.placesOfLanding);

export const selectTepSpecies = createSelector(selectReferenceState, state => state.species.tep);

export const selectNonTepSpecies = createSelector(selectReferenceState, state => state.species.nontep);

export const selectReceivers = createSelector(selectReferenceState, state => state.receivers);

export const selectAreas = createSelector(selectReferenceState, state => state.areaList);

export const selectBlacklip = createSelector(
  selectNonTepSpecies,
  species => species
    .find(({speciesId}) => equals(speciesId, blacklipSpeciesId)),
);

export const selectGreenlip = createSelector(
  selectNonTepSpecies,
  species => species
    .find(({speciesId}) => equals(speciesId, greenlipSpeciesId)),
);
