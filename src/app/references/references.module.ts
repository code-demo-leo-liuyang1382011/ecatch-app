import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { ReferencesEffects } from './references.effects';
import * as fromReferences from './references.reducer';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    StoreModule.forFeature(fromReferences.referencesFeatureKey, fromReferences.reducer),
    EffectsModule.forFeature([ReferencesEffects]),
  ],
})
export class ReferencesModule { }
