import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IReference } from 'fisher-model';
import { Observable } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { EnvsService } from '../shared/envs.service';

@Injectable({
  providedIn: 'root',
})
export class ReferencesService {

  constructor(private http: HttpClient, private envsService: EnvsService) {
  }

  public load(): Observable<IReference> {
    return this.url()
      .pipe(
        switchMap(url => this.http.get<IReference>(url)),
      );
  }

  private url = () => this.envsService.apiUrl()
    .pipe(map(apiUrl => `${apiUrl}/references`))
}
