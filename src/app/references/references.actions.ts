import { createAction, props } from '@ngrx/store';
import { IReference } from 'fisher-model';

export const loadReferences = createAction(
  '[References] Load References',
);

export const loadReferencesSuccess = createAction(
  '[References] Load References Success',
  props<{ data: IReference }>(),
);

export const loadReferencesFailure = createAction(
  '[References] Load References Failure',
);
