import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { IState } from '../settings/settings.reducer';
import { bootstrapApp } from './root.actions';

@Injectable({
  providedIn: 'root',
})
export class RootActionDispatcher {

  constructor(private store: Store<IState>) {
  }

  public bootstrapApp(): void {
    this.store.dispatch(bootstrapApp());
  }
}
