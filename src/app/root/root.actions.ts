import { createAction } from '@ngrx/store';

export const clearCache = createAction(
  '[Root] Clear Cache',
);

export const bootstrapApp = createAction(
  '[Root] Bootstrap App',
);
