import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { HeaderModule } from '../header/header.module';
import { SearchNSelectComponent } from './search-n-select.component';

@NgModule({
  declarations: [SearchNSelectComponent],
  imports: [
    IonicModule,
    CommonModule,
    HeaderModule,
  ],
  exports: [SearchNSelectComponent],
  entryComponents: [SearchNSelectComponent],
})
export class SearchNSelectModule {}
