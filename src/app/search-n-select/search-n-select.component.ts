import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { isNil } from '../ramda-functions';

@Component({
  selector: 'ecatch-search-n-select',
  templateUrl: './search-n-select.component.html',
  styleUrls: ['./search-n-select.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SearchNSelectComponent implements OnInit {

  // tslint:disable-next-line:no-any
  public items: any[];
  // tslint:disable-next-line:no-any
  public displayItems: any[];
  public searchAttributeList: string[];
  // tslint:disable-next-line:no-any
  public selectedItem: any;
  public minCharacters = 1;
  // tslint:disable-next-line:no-any
  public displayLabels: any;
  public placeholderTxt: string;
  public modalTitle: string;
  public itemNotFoundTxt: string;
  public sortOrder: string[];
  public textWrap: boolean;

  constructor(private navParams: NavParams, private modalController: ModalController) {
  }

  public ngOnInit() {
    this.initList();
  }

  // tslint:disable-next-line:no-any
  public search(ev: any) {
    // Reset records back to all of the records
    this.initList();

    // set val to the value of the searchbar
    const val = ev.target.value;

    // if the value is an empty string don't filter the records
    if (val && val.length >= this.minCharacters && val.trim() !== '') {
      this.displayItems = this.filterDisplayItems(this.displayItems, val, this.searchAttributeList);
    }
  }

  public initList() {
    this.items = this.navParams.get('items');
    this.displayItems = this.items.slice();
    this.displayLabels = this.navParams.get('display');
  }
  // tslint:disable-next-line: no-feature-envy no-any
  private filterDisplayItems(displayItems: any[], keyword: string, searchAttributeList: string[]) {
    return  displayItems.filter((item) => {
      for (const attribute of searchAttributeList) {
        if (
          !isNil(item[attribute]) &&
          item[attribute].toString().toLowerCase().indexOf(keyword.toLowerCase()) > -1
        ) {
          return true;
        }
      }

      return false;
    });
  }

  public findSortOrder(sortOrder: string[], defaultSortOrder): string[] {
    return !isNil(sortOrder) ? sortOrder : [defaultSortOrder];
  }

  public close() {
    this.modalController.dismiss(this.selectedItem);
  }

  // tslint:disable-next-line:no-any
  public select(item: any) {
    this.selectedItem = item;
  }

}
