// tslint:disable-next-line:interface-name
export interface SearchNSelect {
  // tslint:disable-next-line:no-any
  items: any[];
  searchAttributeList: string[];
  minCharactersBeforeSearch: number;
  placeholderTxt: string;
  modalTitle: string;
  itemNotFoundTxt: string;
  sortOrder?: string[];
  display: { mainLabel: string, typeLabel?: string, subLabel1?: string, subLabel2?: string, bracketedLabel?: string };
  isLarge?: boolean;
  textWrap?: boolean;
}
