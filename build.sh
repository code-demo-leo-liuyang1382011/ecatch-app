#!/bin/bash
function build-app {
    echo "run npm i"
    npm i

    echo "run update-environments"
    npm run update-environments
    
    echo "run build-prod"
    npm run build-prod || { echo "build failed"; exit 1; }
}

build-app || { echo "build-app failed"; exit 1; }
