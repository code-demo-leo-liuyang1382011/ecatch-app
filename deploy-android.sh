#!/bin/bash

WHO=$(whoami)

echo "setup nvm as $WHO"

source ~/.bash_profile

nvm use

cd fastlane
echo "fastlane upload to itunes"
echo "fastlane upload to google playstore"
fastlane supply --apk app-release.apk --track alpha || { echo "Google playstore upload failed"; exit 1; }


