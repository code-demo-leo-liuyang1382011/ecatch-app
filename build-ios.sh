#!/bin/bash

WHO=$(whoami)

echo "setup nvm as $WHO"
source ~/.bash_profile
nvm use

echo "npm install"
npm i

echo "npx cap sync ios"
npx cap sync ios

echo "remove existing ipa"
rm -rf fastlane/*.ipa

mkdir -p fastlane

echo "Unlock keychain"
/usr/bin/security unlock-keychain -p Rimmer9000 ~/Library/Keychains/login.keychain

echo "add build number if running in jenkins BUILD_NUMBER: $BUILD_NUMBER"
if [ -n "$BUILD_NUMBER" ]; then
    echo "$BUILD_NUMBER is set, run update config"
    npm run update-config
fi

echo "build release"
cd ios/App
./build.sh Release  || { echo "build failed"; exit 1; }

echo "Copying ipa to fastlane folder"
cp "build/App.ipa" ../../fastlane/ || { echo "cp failed"; exit 1; }

echo "Successfully compeleted the ios build!"


