#/bin/bash
source .env
source ./cicd-commons/node-commons.sh
source ./cicd-commons/build-commons.sh
source ./cicd-commons/deploy-commons.sh

DIST_FOLDER="dist"
TARGET_FOLDER="admin-webapp"

echo "Current version: ${WEBAPP_VERSION}"

cd $DIST_FOLDER/$TARGET_FOLDER
pwd=`pwd`
echo "Current folder: ${pwd}"

echo "Zipping the folder: ${DIST_FOLDER}/${TARGET_FOLDER}"
zipit ${WEBAPP_ZIP} .

S3_PATH="s3://${S3_ARTIFACTS}/${S3_ARTIFACTS_PREFIX}"
echo "Uploading the zip file ${WEBAPP_ZIP} the ${S3_PATH}"

uploadCmd="aws s3 cp ${WEBAPP_ZIP} ${S3_PATH}/${WEBAPP_ZIP} --profile ${AWS_PROFILE}"
echo $uploadCmd
eval $uploadCmd

echo "Removing the zip"
rm -rf ${WEBAPP_ZIP}

cd ../..




