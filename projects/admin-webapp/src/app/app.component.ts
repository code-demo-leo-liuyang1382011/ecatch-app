import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { IState as UserProfileState } from '../../../../src/app/login/login.reducer';
import { logout } from '../../../../src/app/login/logout.actions';
import { RootActionDispatcher } from '../../../../src/app/root/root.dispatcher';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  public title = 'admin-webapp';

  constructor(private rootActionDispatcher: RootActionDispatcher,
              private loginStore: Store<UserProfileState>) {
    this.initializeApp();
  }

  private async initializeApp() {
    this.rootActionDispatcher.bootstrapApp();
  }

  public logout() {
    this.loginStore.dispatch(logout());
  }
}
