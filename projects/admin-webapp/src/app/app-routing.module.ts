import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TripManagerComponent } from './trip-manager/trip-manager.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full',
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then((m) => m.LoginModule),
  },
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then((m) => m.HomeModule),
  },
  {
    path: 'trip-manager',
    component: TripManagerComponent,
  },
  {
    path: 'trip',
    loadChildren: () => import('../../../../src/app/trip/trip.module')
      .then((m) => m.TripModule),
  },
  {
    path: 'landing',
    loadChildren:
      () => import('../../../../projects/fisher-app/src/app/landing/landing.module').then((m) => m.LandingModule),
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {
}
