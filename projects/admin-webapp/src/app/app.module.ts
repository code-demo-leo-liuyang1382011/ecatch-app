import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicModule } from '@ionic/angular';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { Deploy } from 'cordova-plugin-ionic/dist/ngx';
import { AppVersionModule } from '../../../../src/app/app-version/app-version.module';
import { AuthModule } from '../../../../src/app/auth/auth.module';
import { HeaderModule } from '../../../../src/app/header/header.module';
import { ReferencesModule } from '../../../../src/app/references/references.module';
import { RockLobsterFishingActivityModule } from '../../../../src/app/rock-lobster-fishing-activity/rock-lobster-fishing-activity.module';
import { SettingsModule } from '../../../../src/app/settings/settings.module';
import { TripHomeAbaloneModule } from '../../../../src/app/trip-home-abalone/trip-home-abalone.module';
import { TripHomeGiantCrabModule } from '../../../../src/app/trip-home-giant-crab/trip-home-giant-crab.module';
import { TripHomeRockLobsterModule } from '../../../../src/app/trip-home-rock-lobster/trip-home-rock-lobster.module';
import { TripModule } from '../../../../src/app/trip/trip.module';
import { environment } from '../../../../src/environments/environment';
import { CatchDisposalRecordModule } from '../../../fisher-app/src/app/catch-disposal-record/catch-disposal-record.module';
import { PreferencesModule } from '../../../fisher-app/src/app/preferences/preferences.module';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { OperatorsModule } from './operators/operators.module';
import { metaReducers, reducers } from './reducers';
import { TripManagerModule } from './trip-manager/trip-manager.module';
import { UserProfileModule } from './user-profile/user-profile.module';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    TripManagerModule,
    TripModule,
    AppRoutingModule,
    HeaderModule,
    IonicModule.forRoot({
      mode: 'md',
      // navAnimation,
    }),
    AuthModule,
    SettingsModule,
    UserProfileModule,
    EffectsModule.forRoot([]),
    OperatorsModule,
    ReferencesModule,
    PreferencesModule,
    StoreModule.forRoot(reducers, {
      metaReducers,
      runtimeChecks: {
        strictStateImmutability: true,
        strictActionImmutability: true,
        strictStateSerializability: true,
        strictActionSerializability: true,
      },
    }),
    StoreDevtoolsModule.instrument({ maxAge: 25, logOnly: environment.production }),
    AppVersionModule,
    TripHomeAbaloneModule,
    TripHomeRockLobsterModule,
    TripHomeGiantCrabModule,
    RockLobsterFishingActivityModule,
    CatchDisposalRecordModule,
  ],
  providers: [
    Deploy,
    { provide: LocationStrategy, useClass: HashLocationStrategy },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {
}
