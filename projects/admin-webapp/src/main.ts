import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { isDefined } from '../../../src/app/shared/isDefined';
import { environment } from '../../../src/environments/environment';
import { AppModule } from './app/app.module';

if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => {
    console.error(err);
  });

if (environment.production && isDefined(window)) {
  // tslint:disable-next-line:no-empty only-arrow-functions
  window.console.log = function () {
  };
}
