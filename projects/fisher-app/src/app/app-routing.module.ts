import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { LoginComponent } from '../../../../src/app/login/login.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full',
  },
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then((m) => m.HomeModule),
  },
  {
    path: 'settings',
    loadChildren: () => import('./settings-page/settings-page.module').then((m) => m.SettingsPageModule),
  },
  {
    path: 'preferences',
    loadChildren: () => import('./preferences/preferences.module').then((m) => m.PreferencesModule),
  },
  {
    path: 'my-quota',
    loadChildren: () => import('./my-quota/my-quota.module').then((m) => m.MyQuotaModule),
  },
  {
    path: 'trip',
    loadChildren: () => import('../../../../src/app/trip/trip.module')
      .then((m) => m.TripModule),
  },
  {
    path: 'activity-log',
    loadChildren: () => import('./activity-log-page/activity-log-page.module').then((m) => m.ActivityLogPageModule),
  },
  {
    path: 'landing',
    loadChildren: () => import('./landing/landing.module').then((m) => m.LandingModule),
  },
  {
    path: 'abalone-catch-trip-status',
    loadChildren: () =>
      import('./abalone-catch-trip-status/abalone-catch-trip-status.module')
        .then((m) => m.AbaloneCatchTripStatusModule),
  },
  {
    path: 'abalone-catch-reef-weights',
    loadChildren: () =>
      import('./abalone-catch-reef-weights/abalone-catch-reef-weights.module')
        .then((m) => m.AbaloneCatchReefWeightsModule),
  },
  {
    path: 'abalone-catch-reef-weights-summary',
    loadChildren: () =>
      import('./abalone-catch-reef-weights-summary/abalone-catch-reef-weights-summary.module')
        .then((m) => m.AbaloneCatchReefWeightsSummaryModule),
  },
  {
    path: 'abalone-catch-bin-weights',
    loadChildren: () =>
      import('./abalone-catch-bin-weights/abalone-catch-bin-weights.module')
        .then((m) => m.AbaloneCatchBinWeightsModule),
  },
  {
    path: 'abalone-catch-bin-weights-summary',
    loadChildren: () =>
      import('./abalone-catch-bin-weights-summary/abalone-catch-bin-weights-summary.module')
        .then((m) => m.AbaloneCatchBinWeightsSummaryModule),
  },
  {
    path: 'abalone-quota-allocation',
    loadChildren: () =>
      import('./abalone-quota-allocation-page/abalone-quota-allocation-page.module')
        .then((m) => m.AbaloneQuotaAllocationPageModule),
  },
  {
    path: 'catch-disposal-record',
    loadChildren: () =>
      import('./catch-disposal-record/catch-disposal-record-routing.module')
        .then((m) => m.CatchDisposalRecordRoutingModule),
  },
  {
    path: 'rock-lobster-trip-status',
    loadChildren: () =>
      import('../../../../src/app/rock-lobster-trip-status/rock-lobster-trip-status.module')
        .then((m) => m.RockLobsterTripStatusModule),
  },
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
