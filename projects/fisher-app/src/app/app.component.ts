import { Component } from '@angular/core';
import { Plugins } from '@capacitor/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { push } from '../../../../src/app/activity-log/activity-log.actions';
import { IState as activityLogState, selectLogs } from '../../../../src/app/activity-log/activity-log.reducer';
import { logout } from '../../../../src/app/login/logout.actions';
import {
  IState as userProfileState,
} from '../../../../src/app/operator-profile/operator-profile.reducer';
import { RootActionDispatcher } from '../../../../src/app/root/root.dispatcher';
import { SettingsStateSelector } from '../../../../src/app/settings/settings.selector';

@Component({
             selector: 'app-root',
             templateUrl: 'app.component.html',
           })
export class AppComponent {

  public debugEnabled$: Observable<boolean>;

  constructor(
    private userProfileStore: Store<userProfileState>,
    private rootActionDispatcher: RootActionDispatcher,
    private settingsStateSelector: SettingsStateSelector,
    private activityLogStore: Store<activityLogState>,
  ) {
    this.initializeApp();

    this.debugEnabled$ = this.settingsStateSelector.debug();
  }

  private async initializeApp() {
    const { SplashScreen } = Plugins;
    await SplashScreen.hide();

    this.activityLogStore.select(selectLogs)
      .subscribe(_ => {
                   this.activityLogStore.dispatch(push());
                 },
      );

    this.rootActionDispatcher.bootstrapApp();
  }

  public logout() {
    this.userProfileStore.dispatch(logout());
  }

}
