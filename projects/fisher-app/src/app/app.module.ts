import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { ErrorHandler, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import * as Sentry from '@sentry/browser';
import { Deploy } from 'cordova-plugin-ionic/dist/ngx';
import { ActivityLogModule } from '../../../../src/app/activity-log/activity-log.module';
import { AppVersionModule } from '../../../../src/app/app-version/app-version.module';
import { AuthModule } from '../../../../src/app/auth/auth.module';
import { OperatorProfileModule } from '../../../../src/app/operator-profile/operator-profile.module';
import { ReferencesModule } from '../../../../src/app/references/references.module';
import { SettingsModule } from '../../../../src/app/settings/settings.module';
import { SentryErrorHandler } from '../../../../src/app/shared/sentry.service';
import { TimeoutInterceptor } from '../../../../src/app/shared/timeout.interceptor';
import { TripModule } from '../../../../src/app/trip/trip.module';
import { environment } from '../../../../src/environments/environment';
import { AbaloneCatchModule } from './abalone-catch/abalone-catch.module';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PreferencesModule } from './preferences/preferences.module';
import { metaReducers, reducers } from './reducers';

if (environment.production) {
  Sentry.init({
    dsn: 'https://ead579c2cbc74237917d9208ffe9e300@sentry.io/1549529',
  });
}

import { LoginModule } from '../../../../src/app/login/login.module';
import {
  RockLobsterFishingActivityModule,
} from '../../../../src/app/rock-lobster-fishing-activity/rock-lobster-fishing-activity.module';
import { TripHomeAbaloneModule } from '../../../../src/app/trip-home-abalone/trip-home-abalone.module';
import { TripHomeGiantCrabModule } from '../../../../src/app/trip-home-giant-crab/trip-home-giant-crab.module';
import { TripHomeRockLobsterModule } from '../../../../src/app/trip-home-rock-lobster/trip-home-rock-lobster.module';
import { AbaloneQuotaAllocationModule } from './abalone-quota-allocation/abalone-quota-allocation.module';
import { CatchDisposalRecordModule } from './catch-disposal-record/catch-disposal-record.module';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    EffectsModule.forRoot([]),
    IonicModule.forRoot({
      mode: 'md',
      // navAnimation,
    }),
    TripModule,
    AppRoutingModule,
    OperatorProfileModule,
    AuthModule,
    ReferencesModule,
    AppVersionModule,
    SettingsModule,
    ActivityLogModule,
    PreferencesModule,
    AbaloneCatchModule,
    AbaloneQuotaAllocationModule,
    CatchDisposalRecordModule,
    RockLobsterFishingActivityModule,
    TripHomeAbaloneModule,
    TripHomeRockLobsterModule,
    TripHomeGiantCrabModule,
    LoginModule,
    StoreModule.forRoot(reducers, {
      metaReducers,
      runtimeChecks: {
        strictStateImmutability: true,
        strictActionImmutability: true,
        strictStateSerializability: true,
        strictActionSerializability: true,
      },
    }),
    StoreDevtoolsModule.instrument({ maxAge: 25, logOnly: environment.production }),
  ],
  providers: [
    { provide: ErrorHandler, useClass: SentryErrorHandler },
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    { provide: HTTP_INTERCEPTORS, useClass: TimeoutInterceptor, multi: true },
    Deploy,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {
}
