###Ionic
npm install -g ionic
######Current version installed is: 5.2.3

---

###Capacitor
Capacitor is installed locally using dev dependencies.

We will use `npx` to run `cap` commands.

---

###Troubleshooting
If the ios project is already added and you get it as part of your checkout, make sure you run the following command.

`npx cap sync`

If you get an error saying `Unknown installation options: disable_input_output_paths`, 
make sure to update your coco pod version first using `sudo gem install cocoapods`



# eCatch

## Environment Setup
### Ionic Cli Setup

    npm install -g ionic@5.2.5 

(https://ionicframework.com/docs/installation/cli)

### Ionic Environment Setup 

Install [nvm](https://nodesource.com/blog/installing-node-js-tutorial-using-nvm-on-mac-os-x-and-ubuntu/)

Install [git](https://docs.gitlab.com/ee/topics/git/how_to_install_git/)

(https://ionicframework.com/docs/installation/environment)

#### IOS Setup

Install xcode and select command line tools for use by running:

    xcode-select --install
    npm install -g ios-sim
    npm install -g ios-deploy

on "xcode-select: error: tool 'xcodebuild' requires Xcode, but active developer directory '/Library/Developer/CommandLineTools' is a command line tools instance" see https://github.com/ios-control/ios-deploy/issues/327

(https://ionicframework.com/docs/installation/ios)

Capacitor ios development also require you to run:

    sudo gem install cocoapods
    pod install
    pod repo update


(https://capacitor.ionicframework.com/docs/getting-started/dependencies/)


#### Android Setup
Install [Java JDK8](https://github.com/AdoptOpenJDK/homebrew-openjdk)

    brew cask install adoptopenjdk/openjdk/adoptopenjdk8


Install cradle

    brew install gradle

Install [android studio](https://developer.android.com/studio/install#mac)

##### Set the ANDROID_SDK_ROOT environment variable

    export ANDROID_SDK_ROOT=$HOME/Library/Android/sdk
##### Add the Android SDK command-line directories to PATH. Each directory corresponds to the category of command-line tool.
###### avdmanager, sdkmanager
    export PATH=$PATH:$ANDROID_SDK_ROOT/tools/bin
###### adb, logcat
    export PATH=$PATH:$ANDROID_SDK_ROOT/platform-tools
###### emulator
    export PATH=$PATH:$ANDROID_SDK_ROOT/emulator

##### Accept SDK licences

    yes | sudo ~/Library/Android/sdk/tools/bin/sdkmanager --licenses

(https://stackoverflow.com/questions/38096225/automatically-accept-all-sdk-licences)

(https://ionicframework.com/docs/installation/android)

## Install

    git clone git@svgitlab.spatialvision.com.au:vic-fisheries/ecatch/ecatch-app.git
    cd ecatch-app
    nvm use
    npm install
    npx cap sync

## Run ios emulator
    npm run ios
    
## Run ios emulator with livereload
    npm run iosl

### Debug ios
Once an app is running on an iOS device or simulator, it can be debugged in Safari.

Safari has Web Inspector support for iOS simulators and devices. Open the Develop menu and select the simulator or device, then select the Ionic App to open Web Inspector.

(https://ionicframework.com/docs/building/ios#debugging-ios-apps)

## Run android with usb connected device
    npm run android
    
## Run android with usb connected device with livereload
    npm run androidl

### Manual reload
If android doesn't livereload when you save changes run this script: 

For changes of web-based code, run: 

    npm run a-build

For changes to plugins, run: 

    npm run a-build-native

Android Studio should open when the script is finished so that you can manually run the project to see changes. 

### Debug android
Once an app is running on an Android device or emulator, it can be debugged with Chrome DevTools. 

Go to chrome://inspect in Chrome while the simulator is running or a device is connected to the computer and Inspect the app that needs to be debugged.
(https://ionicframework.com/docs/building/android#debugging-android-apps)


## To Update Periodically 

    pod repo update
(https://capacitor.ionicframework.com/docs/getting-started/dependencies/)

To check if there are any new updates to Capacitor itself, run 

    npx cap doctor 
    
to print out the current installed dependencies as well view the latest available.

To update Capacitor Core and CLI:

    npm update @capacitor/cli
    npm update @capacitor/core

To update any or all of the platforms you are using:

    npm update @capacitor/ios
    npm update @capacitor/android
    npm update @capacitor/electron

    (https://capacitor.ionicframework.com/docs/basics/workflow)


## Pulling the environment
Prerequisites
  - Install aws-cli and setup default and vfa profile.
    ```
    aws configure
    aws configure --profile vfa
    ```
Fetching the initial .env to set target env in environment file
    ./get-env.sh -e local 

Creating the environment file
    node scripts/update-environments.js
    
Fetch envs-json.app and copy to assets
    ./get-env-app.sh 
