#!/bin/bash
SERVER="svappdevu18-2.sv.domain"
CURRENT_USER=`whoami`
CREDENTIAL_PATH="/home/gitlab-runner/ecatch/decrypted-envs"


function get-envs() {
    local TARGET_ENV=$1
    local BRANCH=$2
    # Set up the TARGET_ENV value
    if [[ -z "$TARGET_ENV" ]]
    then
      echo "Please provide a TARGET_ENV eg -e dev or -e qa"
      exit 1
    fi

    if [[ -z "$BRANCH" ]]
    then
      echo "Please provid a BRANCH name folder in the env file e.g. -b develop. Available options: [develop, release, master]"
      exit 1
    fi

    if [[ -z "${CREDENTIAL_PATH}" ]]
    then
        echo "Please provide a CREDENTIAL_PATH environment variable"
        exit 1
    fi

    cmd="scp ${USER}@${SERVER}:${CREDENTIAL_PATH}/${BRANCH}/${PROJECT}/${TARGET_ENV}.env ./.envs/${TARGET_ENV}.env"
    if [ $CURRENT_USER == "gitlab-runner" ]
    then
      cmd="cp ${CREDENTIAL_PATH}/${BRANCH}/${PROJECT}/${TARGET_ENV}.env ./.envs/${TARGET_ENV}.env"
    fi

    echo "${cmd}"
    eval $cmd || { echo "$cmd failed"; exit 1; }
}

while [[ $# -gt 1 ]]
do
key="$1"

case $key in
    -e|--env)
    TARGET_ENV="$2"
    shift # past argument
    ;;
    -b|--branch)
    BRANCH="$2"
    shift
    ;;
    -p|--project)
    PROJECT="$2"
    shift
    ;;
    *)
          # unknown option
    ;;
esac
shift # past argument or value
done

echo "Creating envs folder if not existing"
mkdir -p .envs || { echo "Folder created fails"; exit 1; }

get-envs $TARGET_ENV $BRANCH $PROJECT

echo "Copying envs to the $PROJECT root folder"
cp .envs/${TARGET_ENV}.env projects/$PROJECT/.env || { echo "Copy env to root folder fails"; exit 1; }


