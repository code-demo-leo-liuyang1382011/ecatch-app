#!/bin/bash
source .env
source ./cicd-commons/node-commons.sh
source ./cicd-commons/build-commons.sh
source ./cicd-commons/deploy-commons.sh

function deploy {
    VERSION=$1
    ZIP=$VERSION.zip
    UNZIP_FOLDER="www"

    cd artifacts

    echo "deploy $VERSION into $S3_WEBAPP"
    rm -f $VERSION.zip
    S3_PATH=s3://$S3_ARTIFACTS/webapp/$VERSION.zip
    echo "download $ZIP from $S3_PATH"
    aws s3 cp $S3_PATH . --profile vfa || { echo "s3 cp failed"; exit 1; }

    echo "unzip $ZIP"
    unzip -o -q $ZIP -d $UNZIP_FOLDER || { echo "unzip failed"; exit 1; }

    cd ..
    echo "copy env vars"
    cp projects/admin-webapp/.env ./artifacts/$UNZIP_FOLDER || { echo "copy env failed"; exit 1; }

    cd artifacts/$UNZIP_FOLDER

    echo "sync with $S3_WEBAPP"
    aws s3 sync --delete . s3://$S3_WEBAPP/admin --profile ${AWS_PROFILE} || { echo "sync failed"; exit 1; }

    cd ..

    echo "remove $ZIP"
    rm -fr $ZIP || { echo "rm failed"; exit 1; }

    echo "invalidate cloudfront"
    npm run invalidate || { echo "invalidate failed"; exit 1; }

    cd ..
}

echo "WEBAPP_VERSION: $WEBAPP_VERSION"
if [ -z "$WEBAPP_VERSION" ]; then
    echo "WEBAPP_VERSION is not set. Setting the default: $WEBAPP_VERSION"
    exit 1
fi

deploy $WEBAPP_VERSION
