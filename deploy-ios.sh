#!/bin/bash

WHO=$(whoami)

echo "setup nvm as $WHO"

source ~/.bash_profile

nvm use

cd fastlane
echo "fastlane upload to itunes"
fastlane pilot upload || {
  echo "Error uploading IPA to TestFlight "
  exit 1
}
