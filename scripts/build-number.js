const createBuildNumber = (version, buildNumber) => {
    const [MAJOR, MINOR, PATCH] = version.split('.');
    return (+MAJOR) * 18754 + (+MINOR) * 100 + (+PATCH) + (+buildNumber);
}

module.exports = createBuildNumber;
