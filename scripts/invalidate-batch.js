const fs = require('fs')
const fileName = 'invalidation-batch.json'
const invalidationBatch =  {
  "Paths": {
    "Quantity": 1,
    "Items": ["/admin/*"]
  },
  "CallerReference": "ecatch-webapp-invalidation" + new Date().toISOString()
}

fs.writeFileSync(fileName, JSON.stringify(invalidationBatch));