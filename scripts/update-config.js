require('dotenv').config();
const plist = require('plist');

console.log('BUILD_NUMBER: %s', process.env.BUILD_NUMBER);
const createBuildNumber = require('./build-number.js');
const version = require(`${__dirname}/../package.json`).version;
const androidManifestXml = `${__dirname}/../android/app/src/main/AndroidManifest.xml`;
process.env.BUILD_NUMBER = createBuildNumber(version, process.env.BUILD_NUMBER);
console.log('process.env.BUILD_NUMBER: %s', process.env.BUILD_NUMBER);

const xml2js = require('xml2js');
const fs = require('fs');
const R = require('ramda');

const jsToXmlFile = (filename, obj) => {
  const filepath = filename;
  const builder = new xml2js.Builder();
  const xml = builder.buildObject(obj);
  fs.writeFileSync(filepath, xml);
};

const updatePath = (result, path, updateTo) => {
  const pathLens = R.lensPath(path);

  // skip the update if the property hasn't been set
  if (typeof updateTo === 'undefined') {
    return result;
  }

  console.log('updatePath updateTo: %s, lense updateTo: %s',
    updateTo,
    R.view(pathLens, result));
  const updated = R.set(pathLens, updateTo, result);
  console.log('updatePath updated to: %s, ', R.view(pathLens, updated));
  return updated;
};

const updates = {
  buildNumber: 'BUILD_NUMBER',
};

const updateConfigIos = () => {
  const plistPath = `${__dirname}/../ios/App/App/info.plist`;
  const plistData = fs.readFileSync(plistPath).toString();
  const info = plist.parse(plistData);
  info.CFBundleShortVersionString = version;
  info.CFBundleVersion = process.env.BUILD_NUMBER;
  const updatedPlistData = plist.build(info);
  fs.writeFileSync(plistPath, updatedPlistData);
};

const updateConfigAndroid = (updates) => new Promise((resolve, reject) => {
  const parser = new xml2js.Parser();
  const data = fs.readFileSync(androidManifestXml);

  parser.parseString(data, function(err, result) {
    result = updatePath(result, ['manifest', '$', 'android:versionName'], version);
    result = updatePath(result, ['manifest', '$', 'android:versionCode'], updates.buildNumber);

    resolve(result);
  });
});

updateConfigAndroid(R.map(envName => process.env[envName], updates))
  .then(R.curry(jsToXmlFile)(androidManifestXml));

updateConfigIos();
