const project = process.argv[2];
console.log('project: %s', project);
require('dotenv').config({path: `${__dirname}/../projects/${project}/.env`});
const fs = require('fs');
console.log('BUILD_NUMBER: %s', process.env.BUILD_NUMBER);
console.log('TARGET_ENV: %s', process.env.TARGET_ENV);
const createBuildNumber = require('./build-number.js');
const version = require(`${__dirname}/../package.json`).version;
process.env.BUILD_NUMBER = createBuildNumber(version, process.env.BUILD_NUMBER || 0);
console.log('process.env.BUILD_NUMBER: %s', process.env.BUILD_NUMBER);

const env = (target, data) => {
  const pathPrefix = `${__dirname}/`;
  let fileContent = JSON.stringify(data);
  fileContent = 'export const environment = ' + fileContent;
  fs.writeFileSync(pathPrefix + target, fileContent);
};

env(`../src/environments/environment.ts`,
  {
    production: true,
    name: `ecatch ${project}`,
    version: version,
    buildNumber: process.env.BUILD_NUMBER,
    targetEnv: process.env.NAME,
  },
);

