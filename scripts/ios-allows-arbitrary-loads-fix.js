require('dotenv').config();
const plist = require('plist');
const fs = require('fs');
const plistFilePath = `${__dirname}/../platforms/ios/vic-fishing/vic-fishing-Info.plist`;
const plistXML = fs.readFileSync(plistFilePath, 'utf8');
const plistJson = plist.parse(plistXML);
delete plistJson.NSAppTransportSecurity.NSExceptionDomains;
delete plistJson.NSAppTransportSecurity.NSAllowsArbitraryLoads;
plistJson.NSAppTransportSecurity.NSAllowsLocalNetworking = true;
const updatedPlistXML = plist.build(plistJson);
fs.writeFileSync(plistFilePath, updatedPlistXML);
console.log('Successfully updated plist\'s security tags (NSAppTransportSecurity)!');

