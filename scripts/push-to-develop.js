const shell = require('shelljs');
const exec = (cmd) => {
  console.log('exec %s', cmd)
  if (shell.exec(cmd).code !== 0) {
    shell.echo(`Error: ${cmd}`);
    shell.exit(1);
  }
}

const version = require(`../package.json`).version;

exec('cd ../..');

exec('git add .');

exec(`git commit -m 'updated the version to ${version}'`);

exec(`git push origin develop`);

