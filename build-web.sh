#!/bin/bash

WHO=$(whoami)

echo "setup nvm as $WHO"
source ~/.bash_profile

nvm use

echo "npm install"
npm i
npm update fisher-model

while [[ $# -gt 1 ]]
do
key="$1"

case $key in
    -p|--project)
    PROJECT="$2"
    shift
    ;;
    *)
          # unknown option
    ;;
esac
shift # past argument or value
done

echo "npm run update-environments"
npm run update-environments $PROJECT

echo "build ionic app"
ionic build --project=$PROJECT --prod
