#!/bin/bash

WHO=`whoami`

echo "Building android as $WHO"
source ~/.bash_profile
nvm use

./gradlew -Pci --console=plain :app:lintDebug -PbuildDir=lint || { echo "android build lint failed"; exit 1; }
./gradlew assembleRelease || { echo "android build assembleRelease failed"; exit 1; }

ANDROID_VERSION=`find $ANDROID_HOME/build-tools -maxdepth 1 -type d -name '*2*' -print -quit` || { echo "android build no apk found"; exit 1; }

$ANDROID_VERSION/apksigner sign \
--ks-pass pass:eCatch2019 \
--ks ecatch.keystore \
--out app/build/outputs/apk/release/app-release.apk app/build/outputs/apk/release/app-release-unsigned.apk || { echo "android build apksigner failed"; exit 1; }
