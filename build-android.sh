#!/bin/bash

echo "setup nvm as $WHO"
source ~/.bash_profile
nvm use

echo "npm install"
npm i

echo "npx cap sync android"
npx cap sync android

echo "remove existing apk"
rm -rf fastlane/*.apk

echo "add build number if running in jenkins BUILD_NUMBER: $BUILD_NUMBER"
if [ -n "$BUILD_NUMBER" ]; then
    echo "$BUILD_NUMBER is set, run update config"
    npm run update-config
fi

echo "build native android for release"
cd android
./build.sh || { echo "build native android failed"; exit 1; }

echo "Copy to fastlane folder"
cp app/build/outputs/apk/release/app-release.apk ../fastlane || { echo "android build - cp failed"; exit 1; }
