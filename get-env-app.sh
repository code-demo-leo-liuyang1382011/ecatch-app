#!/bin/bash
SERVER="svappdevu18-2.sv.domain"
CURRENT_USER=`whoami`
CREDENTIAL_PATH="/home/gitlab-runner/ecatch/decrypted-envs"
FILE="envs-app.json"

while [[ $# -gt 1 ]]
do
key="$1"

case $key in
    -b|--branch)
    BRANCH="$2"
    shift
    ;;
    -p|--project)
    PROJECT="$2"
    shift
    ;;
    *)
          # unknown option
    ;;
esac
shift # past argument or value
done

if [[ -z "$BRANCH" ]]
then
  echo "Please provid a BRANCH name folder in the env file e.g. -b develop. Available options: [develop, release, master]"
  exit 1
fi

if [[ -z "PROJECT" ]]
then
  echo "Please provid a PROJECT name folder in the env file e.g. -p fisher-app. Available options: [fisher-app, admin-webapp]"
  exit 1
fi

echo "Creating envs folder if not existing"
mkdir -p .envs || { echo "Folder created fails"; exit 1; }

cmd="scp ${USER}@${SERVER}:${CREDENTIAL_PATH}/${BRANCH}/${PROJECT}/${FILE} ./.envs/${FILE}"
if [ $CURRENT_USER == "gitlab-runner" ]
then
  cmd="cp ${CREDENTIAL_PATH}/${BRANCH}/${PROJECT}/${FILE} ./.envs/${FILE}"
fi
echo "$cmd"
eval $cmd || { echo "Copy fails"; exit 1; }

echo "Moving envs"
mv .envs/$FILE projects/$PROJECT/src/assets/ || { echo "Move fails"; exit 1; }

echo "projects/$PROJECT/src/assets/$FILE"
cat projects/$PROJECT/src/assets/$FILE
